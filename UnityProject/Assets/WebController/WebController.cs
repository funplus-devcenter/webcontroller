﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class WebController : MonoBehaviour
{
    UniWebView uniWebView;
    UniWebView backendLoader;
    public string prefix;
    public string uuid;
    [Range(1, 10)]
    public int retry = 3;
    public bool fallback;
    public string fallbackUrl;
    public bool useDirectURL = false;
    public string directURL;
    public bool showOnStart = true;
    public bool fullScreen = true;
    public RectTransform referenceRectTransform;
    public Rect frame;
    public bool backButtonEnable = false;
    public delegate void PageCompleteDelegate(int statusCode, string url);
    public event PageCompleteDelegate preloadCompleteEvent;
    public event PageCompleteDelegate loadCompleteEvent;
    public event PageCompleteDelegate preloadErrorRaiseEvent;
    public event PageCompleteDelegate errorRaiseEvent;
    public bool handleCloseMessage = true;
    public delegate void MessageReceivedDelegate(UniWebViewMessage message);
    public event MessageReceivedDelegate messageReceivedEvent;

    public UniWebView UniWebView { get{ return uniWebView; } }

    private void Awake()
    {
        uniWebView = gameObject.AddComponent<UniWebView>();
        var backendObj = new GameObject("backend");
        backendLoader = backendObj.AddComponent<UniWebView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        InitWebView();

        if (fullScreen)
            uniWebView.Frame = new Rect(0, 0, Screen.width, Screen.height);
        else if (referenceRectTransform != null)
        {
            UniWebView.ReferenceRectTransform = referenceRectTransform;
            UniWebView.UpdateFrame();
        }
        else
        {
            UniWebView.Frame = frame;
        }

        if (showOnStart)
            StartGame();
    }

    public void StartGame()
    {
        uniWebView.Show();
        if (!useDirectURL)
            StartCoroutine(LinkUUID());
        else
            Load(directURL);
    }

    public void Show()
    {
        uniWebView.Show();
    }

    public void Hide()
    {
        uniWebView.Hide();
    }

    void InitWebView()
    {
        uniWebView.OnMessageReceived += OnMessageReceived;
        uniWebView.OnShouldClose += OnShouldClose;
        uniWebView.SetBackButtonEnabled(backButtonEnable);
        uniWebView.OnPageFinished += OnWebViewLoaded;
        UniWebView.OnPageErrorReceived += OnPageErrorReceived;

        backendLoader.OnPageFinished += OnBackendLoaded;
        backendLoader.OnPageErrorReceived += OnBackendErrorReceived;
    }

    public void ClearCache()
    {
        uniWebView.CleanCache();
        backendLoader.CleanCache();
    }

    IEnumerator LinkUUID()
    {
        string url = prefix + uuid;
        Log(url);

        retry = Mathf.Clamp(retry, 1, 10);
        for (int i = 0; i < retry; ++i)
        {
            using (var request = UnityWebRequest.Get(url))
            {
                request.certificateHandler = new AcceptAllCertificate();

                yield return request.SendWebRequest();
                Log(request.responseCode.ToString());
                Log(request.error);
                if (!request.isNetworkError && !request.isHttpError)
                {
                    var real = request.downloadHandler.text;
                    Log(real);
                    Load(real);
                    yield break;
                }
            }
        }

        url = url.Replace("https", "http");
        Log(url);

        retry = Mathf.Clamp(retry, 1, 10);
        for (int i = 0; i < retry; ++i)
        {
            using (var request = UnityWebRequest.Get(url))
            {
                request.certificateHandler = new AcceptAllCertificate();

                yield return request.SendWebRequest();
                Log(request.responseCode.ToString());
                Log(request.error);
                if (!request.isNetworkError && !request.isHttpError)
                {
                    var real = request.downloadHandler.text;
                    Log(real);
                    Load(real);
                    yield break;
                }
            }
        }

        if (fallback)
            Load(fallbackUrl);
    }

    string GetURL(string id)
    {
        return "https://bilibili.com";
    }

    public void CloseApplication()
    {
    #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
    #else
        Application.Quit();
    #endif
    }

    void OnMessageReceived(UniWebView webView, UniWebViewMessage message)
    {
        if (handleCloseMessage && message.Path == "close")
            CloseApplication();
        else
            messageReceivedEvent?.Invoke(message);
    }

    bool OnShouldClose(UniWebView webView)
    {
        return false;
    }

    public void SimulateClose()
    {
        uniWebView.AddJavaScript("function test1(){ location.href = \"uniwebview://close\"; }", (payload) =>
        {
            uniWebView.EvaluateJavaScript("test1();");
        });
    }

    public void Load(string url)
    {
        Show();
        uniWebView.Load(url);
    }

    public void Preload(string url)
    {
        backendLoader.Load(url);
    }

    void Log(string log)
    {
        Debug.Log(log);
    }

    void OnWebViewLoaded(UniWebView webView, int statusCode, string url)
    {
        loadCompleteEvent?.Invoke(statusCode, url);
    }

    void OnBackendErrorReceived(UniWebView webView, int errorCode, string errorMessage)
    {
        preloadErrorRaiseEvent?.Invoke(errorCode, errorMessage);
    }

    void OnPageErrorReceived(UniWebView webView, int errorCode, string errorMessage)
    {
        errorRaiseEvent?.Invoke(errorCode, errorMessage);
    }

    void OnBackendLoaded(UniWebView webView, int statusCode, string url)
    {
        preloadCompleteEvent?.Invoke(statusCode, url);
    }

    class AcceptAllCertificate : CertificateHandler
    {
        protected override bool ValidateCertificate(byte[] certificateData)
        {
            return true;
        }
    }
}
